@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Actions</div>
                <div class="card-body">
                    <div class="admin-nav">
                        <ul>
                            <li><a href="#">Manage Rooms</a></li>
                            <li><a href="#">Manage Guests</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div style="height: 30px;">
            </div>
            <div class="card manage-rooms">
                <div class="card-header">Manage Rooms</div>
                <div class="card-body">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addRoom">Add Room</button>
                    <div class="rooms-table">
                        <table id="roomList" class="table table-bordered table-striped dataTable" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Room Number</th>
                                    <th>Floor Number</th>
                                    <th>Status</th>
                                    <th>Golf View?</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <meta name="csrf-token" content="{{ csrf_token() }}" />
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="addRoom" tabindex="-1" role="dialog" aria-labelledby="addRoomLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Room</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('add_room') }}" id="frmDataAdd">
            @csrf
            <div class="form-group row">
                <label for="roomnum" class="col-sm-2 col-form-label">Room Number</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="roomnum" name="roomnum">
                </div>
            </div>
            <div class="form-group row">
                <label for="floornum" class="col-sm-2 col-form-label">Floor Number</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="floornum" name="floornum">
                </div>
            </div>
            <div class="form-group row">
                <label for="roomstat" class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                    <select class="form-control" id="roomstat" name="roomstat">
                      <option>Available</option>
                      <option>Under Maintenance</option>
                      <option>Booked</option>
                      <option>Reserved</option>
                      <option>Occupied</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="isgolfview" class="col-sm-2 col-form-label">Golf View?</label>
                <div class="col-sm-10">
                  <select class="form-control" id="isgolfview" name="isgolfview">
                      <option>Yes</option>
                      <option>No</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div>
        </form>
      </div>
  </div>
</div>

<div class="modal fade" id="editRoom" tabindex="-1" role="dialog" aria-labelledby="editRoomLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Room</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="frmDataEdit">
            @csrf
                  <input type="text" class="form-control" id="id" name="id" style="display:none;">

            <div class="form-group row">
                <label for="roomnum" class="col-sm-2 col-form-label">Room Number</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="roomnum_edit" name="roomnum">
                </div>
            </div>
            <div class="form-group row">
                <label for="floornum" class="col-sm-2 col-form-label">Floor Number</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="floornum_edit" name="floornum">
                </div>
            </div>
            <div class="form-group row">
                <label for="roomstat" class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                    <select class="form-control" id="roomstat_edit" name="roomstat">
                      <option>Available</option>
                      <option>Under Maintenance</option>
                      <option>Booked</option>
                      <option>Reserved</option>
                      <option>Occupied</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="isgolfview" class="col-sm-2 col-form-label">Golf View?</label>
                <div class="col-sm-10">
                  <select class="form-control" id="isgolfview_edit" name="isgolfview">
                      <option>Yes</option>
                      <option>No</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
              </div>
            </div>
        </form>
      </div>
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    table = $('#roomList').DataTable({
        stateSave : true,
        responsive : true,
        processing : true,
        serverSide : true,
        responsive: true,
        autoWidth: false,
        ajax : "{{ route('list_rooms') }}",
        columns : [
            {data: 'room_number', name: 'room_number'},
            {data: 'floor_number', name: 'floor_number'},
            {data: 'status', name: 'status'},
            {data: 'is_golfview', name: 'is_golfview', render: function(data, type, row){
                if (data == true) {
                    return "Yes";
                } else if (data == false) {
                    return "No";
                }
            }},
            {data: 'action', orderable : false ,searchable: false, render: function (data, type, row) {
                return '<button type="button" class="btn btn-success btnEdit" data-id='+row.id+'>Edit</button> <button type="button" class="btn btn-danger btnDelete" data-id='+row.id+'>Delete</button>'
                }
            },
        ],
        order: [[0, 'desc']]
    });

    $('#roomList').on('click','.btnEdit[data-id]', function(e)
    {
        e.preventDefault();
        var selectedID = $(this).attr('data-id');
        var url = globalUrl+"/admin/edit/room/"+selectedID;
        $.ajax({
            url:url,
            type:'GET',
            dataType:'JSON',
            success:function(data)
            {   
                $('#id').val(data[0].id);
                $('#roomnum_edit').val(data[0].room_number);
                $('#floornum_edit').val(data[0].floor_number);
                $('#roomstat_edit').val(data[0].status);
                $('#isgolfview_edit').val(data[0].is_golfview);
                $('#editRoom').modal('show');
            }
        })
    });

    $('#frmDataEdit').on('submit',function(e){
        e.preventDefault();
        $(this).find("button[type='submit']").prop('disabled',true);
        $.ajaxSetup({
            headers: 
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var selectedID = $('#id').val();
        var frm = $('#frmDataEdit');
        var frmData = frm.serialize();
        var url = globalUrl+"/admin/update/room/"+selectedID;
        $.ajax({
            url: url,
            type: "PUT",
            datatype: 'JSON',
            data: frmData,
            success:function(data){
                $('#editRoom').modal('hide');
                frm.trigger('reset');
                table.ajax.reload(null,false);
                swal('success!','Successfully Added','success');
                location.reload();
            }
        });
    });

    $("#roomList").on("click",'.btnDelete[data-id]', function (e) {
        e.preventDefault();
        selectedID = $(this).attr('data-id');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = globalUrl+"/admin/room/delete/"+selectedID;
        $.ajax({
            type: 'DELETE',
            data:{_method:"DELETE"},
            url: url,
            dataType: 'JSON',
            success: function (data) {
                table.ajax.reload(null,false);
                location.reload();
                swal('success!','Successfully Deleted','success');
            }
        });
    });

})
</script>
@endsection
