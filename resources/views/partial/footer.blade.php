<script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<!-- Datatables -->
<script src="{{ url('https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ url('https://datatables.yajrabox.com/js/handlebars.js') }}"></script>
<script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.0.0/jquery.mark.min.js') }}"></script>
<!--Sweet Alert-->
<script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js') }}"></script>
<!--Select2-->
<script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js') }}"></script>
<script>
    var globalUrl = "{{url('/')}}";
</script>