@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Search Rooms</div>
                <div class="card-body">
                    <div class="customer-form">
                        <form method="POST" action="{{ route('search_rooms') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="floor" class="col-sm-2 col-form-label">Input Floor</label>
                                <div class="col-sm-10">
                                  <select class="form-control" id="floor" name="floor">
                                    @for($init = 1; $init <= 10; $init++) 
                                      <option>{{$init}}</option>
                                    @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="floor" class="col-sm-2 col-form-label">Golf View Rooms?</label>
                                <div class="col-sm-10">
                                  <select class="form-control" id="golfviewrooms" name="golfviewrooms">
                                      <option>Yes</option>
                                      <option>No</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Search</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="height: 30px;"></div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card show-rooms">
                    <div class="card-body">
                        <div class="rooms-table">
                            <table id="roomListHome" class="table table-bordered table-striped dataTable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Room Number</th>
                                        <th>Floor Number</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <meta name="csrf-token" content="{{ csrf_token() }}" />
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    table = $('#roomListHome').DataTable({
        stateSave : true,
        responsive : true,
        processing : true,
        serverSide : true,
        responsive: true,
        autoWidth: false,
        ajax : "{{ route('results') }}",
        columns : [
            {data: 'room_number', name: 'room_number'},
            {data: 'floor_number', name: 'floor_number'},
            {data: 'action', orderable : false ,searchable: false, render: function (data, type, row) {
                return '<button type="button" class="btn btn-success btnBook" data-id='+row.id+'>Book Room</button>'
                }
            },
        ],
        order: [[0, 'desc']]
    });

})
</script>
@endsection