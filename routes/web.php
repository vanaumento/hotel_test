<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin/show/room','RoomController@listRooms')->name('list_rooms');

Route::get('/admin/edit/room/{id}','RoomController@editRoom');

Route::post('/admin/add/room','RoomController@addRoom')->name('add_room');

Route::put('/admin/update/room/{id}','RoomController@updateRoom');

Route::delete('/admin/room/delete/{id}','RoomController@deleteRoom');

Route::post('/search/rooms/show','RoomController@searchRooms')->name('search_rooms');

Route::get('/search/rooms/results','RoomController@roomResults')->name('results');
