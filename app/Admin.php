<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = "admin";
    
    protected $fillable = [
        'users_id'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
