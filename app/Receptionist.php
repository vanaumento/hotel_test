<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receptionist extends Model
{
    protected $table = "receptionist";
    
    protected $fillable = [
        'users_id'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
