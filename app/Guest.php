<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $table = "guest";

    protected $fillable = [
        'users_id'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    
}
