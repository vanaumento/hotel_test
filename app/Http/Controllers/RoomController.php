<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;

use App\Room;

class RoomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('roomResults','searchRooms');
    }

    public function listRooms()
    {
    	$select = Room::get();
 
    	if(request()->ajax()){
    		return Datatables::of($select)
    		->make(true);
    	}
    	return view('home');
    }

    public function addRoom(Request $request)
    {
    	$newBool = false;

    	if( $request->input('isgolfview') === 'Yes'){
    		$newBool = true;
    	} else if($request->input('isgolfview') === 'No'){
    		$newBool = false;
    	}

    	$room = new Room([
    		'room_number' => $request->input('roomnum'),
    		'floor_number' => $request->input('floornum'),
    		'status' => $request->input('roomstat'),
    		'is_golfview' => $newBool,
    	]);

    	$room->save();

    	return \Redirect::back();
    }

    public function editRoom($id)
    {
    	$select = Room::where('id',$id)->get();

    	return response()->json($select);
    }

    public function updateRoom(Request $request, $id)
    {
    	$room = Room::find($id);

    	$newBool = false;

    	if( $request->isgolfview === 'Yes'){
    		$newBool = true;
    	} else if($request->isgolfview === 'No'){
    		$newBool = false;
    	}

        $room->room_number = $request->roomnum;
        $room->floor_number = $request->floornum;
        $room->status = $request->roomstat;    
        $room->is_golfview = $newBool;    
        $room->save();

        return response()->json(array("success"=>true));
    }

    public function deleteRoom($id)
    {
    	$room = Room::find($id);
    	$room->delete();

        return response()->json(['success'=>'Data is successfully deleted']);
    }

    public function searchRooms(Request $request)
    {
		$newBool = false;
		$floor = $request->input('floor');
    	if( $request->input('golfviewrooms') === 'Yes'){
    		$newBool = true;
    	} else if( $request->input('golfviewrooms') === 'No'){
    		$newBool = false;
    	}

    	$select = Room::where('floor_number', $floor)
		    	->where('is_golfview',$newBool)
		    	->where('status','Available')
		    	->get();

    	if(request()->ajax()){
			return Datatables::of($select)
			->make(true);

		}

    	return view('welcome');
    }


    public function roomResults() 
    {
		$selected = Room::where('status','Available')->get();

		if(request()->ajax()){
			return Datatables::of($selected)
			->make(true);
		}
    	return view('welcome');
    }
}
